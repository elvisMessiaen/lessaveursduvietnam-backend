package fr.lessaveursduvietnambackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LessaveursduvietnamBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(LessaveursduvietnamBackendApplication.class, args);
	}

}
