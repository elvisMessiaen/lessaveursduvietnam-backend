package fr.lessaveursduvietnambackend.entity.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * @autor Elvis
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Categories implements Serializable{

	private static final long serialVersionUID = 6780541078278685809L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id_categories;
	private String categoryName ;
	private String description;
	private String photoName ;
	@Lob
	private byte[] picture;
	@OneToMany(mappedBy="id_categories")
	private Collection<Product> product=new ArrayList<Product>();
	
	
	
}
