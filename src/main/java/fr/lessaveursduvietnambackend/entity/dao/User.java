package fr.lessaveursduvietnambackend.entity.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * @autor Elvis
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User implements Serializable , UserDetails{
	private static final long serialVersionUID = 6763250228730247954L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id_user;
	private String firstName;
	private String lastName;
	private String phone;
	
	@Column(unique = true)
	private String mail;
	@Column(unique = true)
	private String username;
	private String password;

	@OneToMany(mappedBy = "id_user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Address> adresse;
	
	
	@OneToMany(mappedBy = "id_user", cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<Order> order;


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}


	@Override
	public String getUsername() {
		return username;
	}


	@Override
	public boolean isAccountNonExpired() {
		return false;
	}


	@Override
	public boolean isAccountNonLocked() {
		return false;
	}


	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}


	@Override
	public boolean isEnabled() {
		return false;
	}
	
}
