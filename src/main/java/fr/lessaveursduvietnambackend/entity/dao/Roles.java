package fr.lessaveursduvietnambackend.entity.dao;
/*
 * @autor Elvis
 */

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor 
@NoArgsConstructor 
@Entity 
public class Roles {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE) 
	private Long id;
	private String roleName;
	
	@ManyToOne() 
	@JoinColumn(name = "id_user")
	private User user;
 
}
