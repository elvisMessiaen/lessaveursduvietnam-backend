package fr.lessaveursduvietnambackend.entity.dao;

import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/*
 * @autor Elvis
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id_product;
	@Column(unique = true)
	private String name;
	private String description;
	private double weight;
	private String originProduct;
	private int quantity;
	private double price;
	private Blob picture;
	
	@ManyToOne @JoinColumn(name="id_user") 
	private Categories Categories;

}

