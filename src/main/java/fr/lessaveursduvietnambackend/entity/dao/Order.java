package fr.lessaveursduvietnambackend.entity.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * @autor Elvis
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Order implements Serializable{

	private static final long serialVersionUID = -4881305738867247003L;
	@Id @GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id_order;
	private Date dateOrder;
	
	@ManyToOne
	@JoinColumn(name="id_user")
	private User user;
	
	@OneToMany
	@JoinColumn(name="id_order_line")
	private Collection<OrderLine> ligneCommandes;
	
}

