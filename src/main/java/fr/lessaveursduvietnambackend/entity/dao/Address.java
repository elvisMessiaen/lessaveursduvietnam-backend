package fr.lessaveursduvietnambackend.entity.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * @autor Elvis
 */

@Data
@AllArgsConstructor 
@NoArgsConstructor 
@Entity 
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE) 
	private Long idAddress; 
	
	@Column(name = "houseNumber", nullable = false) 
	private int houseNumber; 
	
	@Column(name = "street", nullable = false) 
	private String street; 
	
	@Column(name = "postCode", nullable = false) 
	private int postCode; 
	
	@Column(name = "town", nullable = false) 	
	private String town; 
	
	@ManyToOne() 
	@JoinColumn(name = "id_user")
	private User user;
}
